FROM php:latest

MAINTAINER Mohamed El Jemai <mohamed.el.jemai@rwth-aachen.de>

RUN apt-get update -yqq && apt-get install gnupg2 -yqq 
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -

RUN apt-get install git nodejs libcurl4-gnutls-dev libicu-dev libmcrypt-dev \
    libvpx-dev libjpeg-dev libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev \
    libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libtidy-dev -yqq

RUN docker-php-ext-install mbstring pdo_mysql curl json intl gd xml zip bz2 opcache

RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

ADD composer /usr/bin/composer	

WORKDIR /var/www
CMD ["/usr/local/bin/php"]
